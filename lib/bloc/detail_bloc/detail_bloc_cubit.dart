import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'detail_bloc_state.dart';

class DetailBlocCubit extends Cubit<DetailBlocState> {
  DetailBlocCubit() : super(DetailBlocInitial());

  void fetchMovieInfo() async {
    emit(DetailBlocInitial());
    
  }
}
