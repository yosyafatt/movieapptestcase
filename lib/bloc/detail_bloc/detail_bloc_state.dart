part of 'detail_bloc_cubit.dart';

abstract class DetailBlocState extends Equatable {
  const DetailBlocState();

  @override
  List<Object> get props => [];
}

class DetailBlocInitial extends DetailBlocState {}
