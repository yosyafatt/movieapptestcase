import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:majootestcase/utils/db_helper.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void toLogin() => emit(AuthBlocLoginState());
  void toRegister() => emit(AuthBlocRegisterState());

  void registerUser(User user) async {
    final db = DbHelper.instance;
    var res = await db.isExists(user.toJson());
    print(res);
    if (res > 0) {
      emit(AuthBlocRegisterState(data: user));
      return;
    }

    res = await db.insert(user.toJson());

    if (res <= 0) {
      emit(AuthBlocRegisterState());
      return;
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    await sharedPreferences.setBool("is_logged_in", true);
    String data = user.toJson().toString();
    sharedPreferences.setString("user_value", data);
    emit(AuthBlocSuccessState());
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoggedInState());
    });
  }

  void loginUser(User user) async {
    final db = DbHelper.instance;
    final res = await db.find(user.toJson());
    if (res.isEmpty) {
      emit(AuthBlocErrorState('Login gagal, periksa kembali inputan anda'));
      emit(AuthBlocLoginState());
      return;
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);
    // inspect(sharedPreferences);
    // emit(AuthBlocLoginState());
    String data = user.toJson().toString();
    // inspect(data);
    sharedPreferences.setString("user_value", data);
    // inspect(sharedPreferences);
    // emit(AuthBlocLoginState());
    emit(AuthBlocSuccessState(success: 'Login berhasil'));
    Future.delayed(Duration(seconds: 2), () {
      emit(AuthBlocLoggedInState());
    });
  }

  void logout() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    final res = await sp.setBool("is_logged_in", false);
    print(res);
    emit(AuthBlocLoginState());
  }
}
