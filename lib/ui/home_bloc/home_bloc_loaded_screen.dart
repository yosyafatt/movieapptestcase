import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Movie App'),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              context.bloc<AuthBlocCubit>().logout();
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return movieItemWidget(context, data[index]);
        },
        padding: EdgeInsets.all(8),
      ),
    );
  }

  Widget movieItemWidget(BuildContext context, Results data) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => DetailPage(data)),
        );
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        elevation: 6,
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: Image.network(
                "https://image.tmdb.org/t/p/w500/" + data.posterPath,
                height: 180,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(
                  data.title == null ? '' : data.title,
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
