import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();

      var res = await dio.get('');
      if (res.statusCode == 200) {
        return MovieModel.fromJson(res.data);
      }
      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
