import 'dart:io';
import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DbHelper {
  static Database _database;

  static const String DB_Name = 'majoo.db';
  static const int DB_Version = 1;

  static const String Table_Name = 'user';
  static const String C_UserID = 'id';
  static const String C_UserName = 'username';
  static const String C_UserEmail = 'email';
  static const String C_UserPassword = 'password';

  DbHelper._constructor();
  static final DbHelper instance = DbHelper._constructor();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = join(dir.path, DB_Name);

    final db = await openDatabase(
      path,
      version: DB_Version,
      onCreate: _onCreate,
    );

    return db;
  }

  _onCreate(Database d, int v) async {
    await d.execute('''
      CREATE TABLE $Table_Name (
        $C_UserID INTEGER,
        $C_UserName TEXT NOT NULL,
        $C_UserEmail TEXT NOT NULL,
        $C_UserPassword TEXT NOT NULL,
        PRIMARY KEY ($C_UserID)
      )
    ''');
  }

  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(Table_Name, row);
  }

  Future<List<Map<String, dynamic>>> queryAll() async {
    Database db = await instance.database;
    return await db.query(Table_Name);
  }

  Future<int> update(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.update(Table_Name, row,
        where: "$C_UserID = ?", whereArgs: [row[C_UserID]]);
  }

  Future<int> delete(int id) async {
    Database db = await instance.database;
    return await db.delete(Table_Name, where: '$C_UserID = ?', whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> find(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.query(Table_Name,
        where: '$C_UserEmail = ? AND $C_UserPassword = ?',
        whereArgs: [row[C_UserEmail], row[C_UserPassword]]);
  }

  Future<int> isExists(Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.query(Table_Name,
        where: '$C_UserEmail = ?',
        whereArgs: [row[C_UserEmail]]).then((lists) => lists.length);
  }
}
